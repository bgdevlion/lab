(function(exp) {
    var fs = require('fs');
    var path = require('path');
    var filename = path.join(__dirname, "/../database/db.db");

    exp.save = function (obj, callback) {
        exp.get(function (err, result) {
            if(result === null){
                result = new Array();
            }
            result.push(obj);
            console.log("saving error: " + err + "result: " + JSON.stringify(result) + " obj: " + JSON.stringify(obj));

            writeFile(result);
        });

        function writeFile(arr) {
            var str = JSON.stringify(arr);
            fs.writeFile(filename, str, function(err) {
                if (err) {
                    console.log(err);
                    callback(err, null);
                } else {
                    console.log("The file was saved!");
                    callback(null, JSON.parse(str));
                }
            });
        }
    };

    exp.get = function(callback) {
        fs.readFile(filename, function (err, data) {
            if (err) {
                console.log("error: " + err);
                callback(err, null);
            } else {
                callback(null, JSON.parse(data));
            }
        });
    }

})(exports);

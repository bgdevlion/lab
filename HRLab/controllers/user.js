(function(exp){
	//res.render('home/index.vash', {title: "index"}); for absolute file
			
	exp.config = function(app){	
		app.get('/user', function(req, res){
			var repo = require('../services/fileRepository.js');
			
			repo.get(function(err, result){
				res.render('user', { title: "User", data: result});			
			});							
		});				
		
		app.post('/user', function(req, res){
			var repo = require('../services/fileRepository.js');			
			var user = req.body;
			
			repo.save(user, function(err, result){
				res.render('user', { title: "User", data: result});			
			});			
		});	
	};
})(exports);
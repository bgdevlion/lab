(function (exp) {
	exp.config = function (app) {
		configRoute();


	
		function configRoute(){
			// development error handler
			// will output stacktrace in error property
			if (app.get('env') === 'development') {
				app.use(function(err, req, res, next) {
					res.render('error', {
						message: err.message,
						error: err
					});
				});
			}
		}
	};
})(exports);
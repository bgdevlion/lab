(function (exp) {
	var indexController = require('../controllers/index');
	var userController = require('../controllers/user');
	

	exp.config = function (app) {	
	
		indexController.config(app);
		userController.config(app);
		
		// catch 404 and forwarding to error handler
		// this kick in when none of the above routes match the reqest, hence must be 404
		app.use(function(req, res, next) {
			var err = new Error('Not Found');
			err.status = 404;
			next(err);
		});
			
		// production error handler
		// no stacktraces leaked to user
		// all server error will be handled here, we just render error page with error property
		app.use(function(err, req, res, next) {
			res.render('error', {
				message: err.message,
				error: {}
			});
		});
	};
})(exports);
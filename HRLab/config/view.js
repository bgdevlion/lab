(function (exp) {
  var path = require('path');

  exp.config = function (app) {
	app.set('views', path.join(__dirname, '../views'));
	app.set('view engine', 'vash'); 
  };

})(exports);
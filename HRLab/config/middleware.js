(function (exp) {
	var path = require('path');
	var favicon = require('static-favicon');
	var logger = require('morgan');
	var cookieParser = require('cookie-parser');
	var bodyParser = require('body-parser');
	var express = require('express');
	
	exp.config = function (app) {
		app.use(favicon());
		app.use(logger('dev'));
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded());
		app.use(cookieParser());
		app.use(require('less-middleware')({ src: path.join(__dirname, '../public') }));
		app.use(express.static(path.join(__dirname, '../public')));
		app.use(app.router);
	};
})(exports);
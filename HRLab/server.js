var express = require('express');
var view = require('./config/view.js');
var middleWare = require('./config/middleware.js');
var route = require('./config/route.js');
var dev = require('./config/dev.js');

//create express app
var app = express();

//config app settings
view.config(app);
middleWare.config(app);
route.config(app);
dev.config(app);

//export app, so node will forward requests to expperss
module.exports = app;
